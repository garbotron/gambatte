Build instructions for win32:

1. Get Python 2.7 and Scons and add both to your PATH.
2. Get MinGW and also add it to your PATH (e.g. "C:\MinGW\bin").
3. Run 'scons' from the 'libgambatte' directory.
4. Run 'scons' from the 'gambatte_4p_special' directory.

Running instructions:

gambatte_4p_special.exe <gb-rom-path> <player1-controller-index> [<player2..4-controller-index>]

Use 'detect-controllers.bat' to figure out each players' controller index.
